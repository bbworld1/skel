class TemplateNotExistsError(Exception):
    pass

class UtilityNotExistsError(Exception):
    pass
