import setuptools
from {{ project_name }}.version import VERSION

setuptools.setup(
    version=VERSION
)
